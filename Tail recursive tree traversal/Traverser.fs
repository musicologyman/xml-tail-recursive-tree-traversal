﻿module Traverser

open System
open System.Diagnostics
open System.Xml
open System.Xml.Linq

type Element = | Element of string * Element list

let rec walkXmlPath ( x : XElement ) =
   
   let st = new StackTrace()
   printfn "XElement name: %s, FrameCount: %d" x.Name.LocalName st.FrameCount

   // Check if a node has a child/children nodes
   let children =
       x.Elements()
       |> Seq.map walkXmlPath
       |> Seq.toList

   // If there are children nodes, then iterate over them

   // Inside the iteration, do some necessary operations and call 'def walkXmlPath(...)' until the complete tree walk 

   // Return some processed nodes' extracts      
   Element (x.Name.LocalName, children)

let toElement (s : string) =
    s |> XName.Get |> (fun n -> new XElement(n))

let inline (+^) (x : XElement) (s : string) =
    x.Add(s |> toElement)
    x

let inline (+) (x : XElement) (s : string) =
    let c = s |> toElement
    x.Add(c)
    c

let inline (++) (x : XElement) (names : string seq) =
    names |> Seq.iter (fun n -> x + n |> ignore)
    x

let inline (^+) (x : XElement) (s : string) =
    (x +^ s).Parent

[<EntryPoint>]
let main args =
    let e = "a" |> toElement
    (e +^ "b" + "b" +^ "c" +^ "c" + "c" + "d") |> ignore
    e
    |> walkXmlPath
    |> printfn "%A"
    Console.ReadLine() |> ignore
    0